import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-part-one',
  templateUrl: './part-one.component.html',
  styleUrls: ['./part-one.component.css']
})
export class PartOneComponent implements OnInit {

  heights: Array<number> = [];
  count: number = 0;
  heightData = this.formBuilder.group({
    height: undefined
  });

  ngOnInit(): void {
    // const arr = [1,6,3,3,3,6,1];
    // console.log(this.getPossibleNumOfCastles(arr));
  }

  constructor(private formBuilder: FormBuilder) {}

  getPossibleNumOfCastles(heights: number[]): number {

    if (heights.length < 3) { return heights.length; }
  
    // remove duplicates from flat height
    heights = this.filterSameConsecutiveHeights(heights);
    
    this.count = 2;
    for (let i=1; i<heights.length-1; i++) {

      // check for peak
      if (heights[i] > heights[i-1] && heights[i] > heights[i+1]) {
        this.count++;
      }

      // check for valley
      if (heights[i] < heights[i-1] && heights[i] < heights[i+1]) {
        this.count++;
      }
    }
    return this.count;
  }

  filterSameConsecutiveHeights(heights: number[]) {
    return heights.filter((height, i) => i === 0 || height !== heights[i-1]);
  }

  onSubmit() {
    const height = this.heightData.get('height')!.value;
    this.heights.push(height);
    this.heightData.get('height')!.setValue(undefined);
  }
}
