import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PartOneComponent } from './part-one.component';

describe('PartOneComponent', () => {
  let component: PartOneComponent;
  let fixture: ComponentFixture<PartOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartOneComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should be able to build castles at the biggining/end of the heights', () => {
    expect(component.getPossibleNumOfCastles([1])).toEqual(1);
    expect(component.getPossibleNumOfCastles([1, 1])).toEqual(2);
  });

  it ('should remove same consecutive heights', () => {
    expect(component.filterSameConsecutiveHeights([1,3,3,3,2,1,1,1,2])).toEqual([1,3,2,1,2]);
  });

  it ('should return possible number of castles when a peak(s) exists', () => {
    expect(component.getPossibleNumOfCastles([1,3,1])).toEqual(3);
    expect(component.getPossibleNumOfCastles([1,3,3,3,1])).toEqual(3);
    expect(component.getPossibleNumOfCastles([1,2,3,4,3,2,1])).toEqual(3);
  });

  it ('should return possible number of castles when a valley exists', () => {
    expect(component.getPossibleNumOfCastles([3,1,3])).toEqual(3);
    expect(component.getPossibleNumOfCastles([3,1,1,1,3])).toEqual(3);
    expect(component.getPossibleNumOfCastles([3,2,1,1,1,2,3])).toEqual(3);
  });

  it ('should return possible number of castles when a peak and a valley exists', () => {
    expect(component.getPossibleNumOfCastles([1,3,2,3,1])).toEqual(5);
    expect(component.getPossibleNumOfCastles([1,3,3,3,2,2,3,3,3,1])).toEqual(5);
  });

  it ('should return possible number of castles when multiple peaks and vallys exist', () => {
    expect(component.getPossibleNumOfCastles([1,3,2,3,1,7,5,7,1])).toEqual(9);
    expect(component.getPossibleNumOfCastles([7,6,5,6,7,3,2,5,6,1])).toEqual(6);
  });
});
