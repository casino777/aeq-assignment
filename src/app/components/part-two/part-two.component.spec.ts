import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PartTwoComponent, Transformer } from './part-two.component';

describe('PartTwoComponent', () => {
  let component: PartTwoComponent;
  let fixture: ComponentFixture<PartTwoComponent>;

  const optimusPrime = new Transformer(
    'Optimus Prime',
    'A',
    3, 3, 3, 3, 9, 3, 3, 3
  );

  const predaking = new Transformer(
    'Predaking',
    'D',
    3, 3, 3, 3, 9, 3, 3, 3
  );

  const strongAutobot = new Transformer(
    'strongAutobot',
    'A',
    10, 10, 10, 10, 10, 10, 10, 10
  );

  const strongDecepticon = new Transformer(
    'strongDecepticon',
    'D',
    10, 10, 10, 10, 10, 10, 10, 10
  );

  const weakAutobot = new Transformer(
    'weakAutobot',
    'A',
    3, 3, 3, 3, 3, 3, 3, 3
  );

  const weakDecepticon = new Transformer(
    'weakDecepticon',
    'D',
    3, 3, 3, 3, 3, 3, 3, 3
  );

  const skillfulAutobot = new Transformer(
    'skillfulAutobot',
    'A',
    3, 3, 3, 3, 4, 3, 3, 10
  );

  const skillfulDecepticon = new Transformer(
    'skillfulDecepticon',
    'D',
    3, 3, 3, 3, 4, 3, 3, 10
  );

  const moderateAutobot = new Transformer(
    'moderateAutobot',
    'A',
    5, 5, 5, 5, 5, 5, 5, 5
  );

  const moderateDecepticon = new Transformer(
    'moderateDecepticon',
    'D',
    5, 5, 5, 5, 5, 5, 5, 5
  );


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartTwoComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return Optimus Prime as a winner', () => {
    const winner = component.getWinner(optimusPrime, strongDecepticon);
    expect(winner).toEqual(optimusPrime);
  });

  it('should return Predaking as a winner', () => {
    const winner = component.getWinner(strongAutobot, predaking);
    expect(winner).toEqual(predaking);
  });

  it('should compare courage and stength points to determine a winner', () => {
    let winner = component.getWinner(strongAutobot, weakDecepticon);
    expect(winner).toEqual(strongAutobot);
    winner = component.getWinner(weakAutobot, strongDecepticon);
    expect(winner).toEqual(strongDecepticon);
  });

  it('should compare skill points to determine a winner', () => {
    let winner = component.getWinner(skillfulAutobot, weakDecepticon);
    expect(winner).toEqual(skillfulAutobot);
    winner = component.getWinner(weakAutobot, skillfulDecepticon);
    expect(winner).toEqual(skillfulDecepticon);
  });
  
  it('should compare overall stats to determine a winner', () => {
    let winner = component.getWinner(moderateAutobot, weakDecepticon);
    expect(winner).toEqual(moderateAutobot);
    winner = component.getWinner(weakAutobot, moderateDecepticon);
    expect(winner).toEqual(moderateDecepticon);
  });

  it('should return undefined in case of draw', () => {
    let winner = component.getWinner(optimusPrime, predaking);
    expect(winner).toEqual(undefined);
    winner = component.getWinner(moderateAutobot, moderateDecepticon);
    expect(winner).toEqual(undefined);
  });

  it('should determine the winning team', () => {
    component.teamAutobot.push(strongAutobot);
    component.teamAutobot.push(moderateAutobot);
    component.teamDecepticon.push(moderateDecepticon);
    component.teamDecepticon.push(weakDecepticon);
    component.startBattle();
    expect(component.winnerTeam).toEqual(component.teamAutobot);
    expect(component.winnerTeamDescription).toEqual('Autobot');

    component.teamDecepticon.push(predaking);
    component.startBattle();
    expect(component.winnerTeam).toEqual(component.teamDecepticon);
    expect(component.winnerTeamDescription).toEqual('Decepticon');
  });

  it('should return draw if two teams have the same points', () => {
    component.teamAutobot.push(strongAutobot);
    component.teamAutobot.push(moderateAutobot);
    component.teamDecepticon.push(strongAutobot);
    component.teamDecepticon.push(moderateAutobot);
    component.startBattle();
    expect(component.winnerTeam).toEqual(undefined);
    expect(component.winnerTeamDescription).toEqual('Draw');
  });

  it('should return number of battles equals to smaller length of team', () => {
    component.teamAutobot.push(strongAutobot);
    component.teamAutobot.push(moderateAutobot);
    component.teamDecepticon.push(moderateDecepticon);
    component.teamDecepticon.push(weakDecepticon);
    component.teamDecepticon.push(predaking);
    component.startBattle();
    expect(component.numberOfFights).toEqual(2);
  });

  it('should return survivors of losing team', () => {
    component.teamAutobot.push(strongAutobot);
    component.teamAutobot.push(moderateAutobot);
    component.teamAutobot.push(weakAutobot);
    component.teamDecepticon.push(moderateDecepticon);
    component.teamDecepticon.push(predaking);
    component.startBattle();
    expect(component.survivors).toEqual([weakAutobot]);
  });
});
