import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-part-two',
  templateUrl: './part-two.component.html',
  styleUrls: ['./part-two.component.css']
})

export class PartTwoComponent {
  teamAutobot: Array<Transformer> = [];
  teamDecepticon: Array<Transformer> = [];
  winnerTeam: any;
  winnerTeamDescription: any;
  survivors: any;
  numberOfFights = 0;
  transformerData = this.formBuilder.group({
    name: '',
    team: '',
    strength: undefined,
    intelligence: undefined,
    speed: undefined,
    endurance: undefined,
    rank: undefined,
    courage: undefined,
    firePower: undefined,
    skill: undefined
  });

  constructor(private formBuilder: FormBuilder) { }

  startBattle() {
    let teamAutobotPoints = 0;
    let teamDecepticonPoints = 0;

    this.sortArrayByRankPerTeam();

    this.numberOfFights = this.teamAutobot.length < this.teamDecepticon.length ?
    this.teamAutobot.length : this.teamDecepticon.length;

    let winner;
    for (let i=0; i<this.numberOfFights; i++) {
      winner = this.getWinner(this.teamAutobot[i], this.teamDecepticon[i]);

      if (!winner) {                            // case of draw
        this.teamAutobot[i].alive = false;
        this.teamDecepticon[i].alive = false;
      } else if (winner.team === 'A') {         // autobot wins
        teamAutobotPoints++;
        this.teamDecepticon[i].alive = false;
      } else {                                  // decepticon wins
        teamDecepticonPoints++;
        this.teamAutobot[i].alive = false;
      }
    }

    if (teamAutobotPoints !== teamDecepticonPoints) {
      if (teamAutobotPoints > teamDecepticonPoints) {
        this.winnerTeam = this.teamAutobot;
        this.winnerTeamDescription = "Autobot"
        this.survivors = this.teamDecepticon.filter(el => el.alive);
      } else if (teamAutobotPoints < teamDecepticonPoints) {
        this.winnerTeam = this.teamDecepticon;
        this.winnerTeamDescription = "Decepticon"
        this.survivors = this.teamAutobot.filter(el => el.alive);
      }
    } else {
      this.winnerTeamDescription = "Draw";
    }
  }

  sortArrayByRankPerTeam() {
    // sort transformers by rank
    this.teamAutobot.sort((a, b) => b.rank - a.rank);
    this.teamDecepticon.sort((a, b) => b.rank - a.rank);
    console.log('autobot: ', this.teamAutobot);
    console.log('decepticon: ', this.teamDecepticon);
  }

  getWinner(autobot: Transformer, decepticon: Transformer): Transformer | undefined {
    if (!(autobot.name === 'Optimus Prime' && decepticon.name === 'Predaking')) {
      if (autobot.name === 'Optimus Prime') {
        return autobot;
      }
      if (decepticon.name === 'Predaking') {
        return decepticon;
      }
    }
    
    const compareCourage = autobot.courage - decepticon.courage;
    const compareStrength = autobot.strength - decepticon.strength;
    const compareSkill = autobot.skill - decepticon.skill;

    if (Math.abs(compareCourage) >= 4 && Math.abs(compareStrength) >= 3) {
      if (compareCourage > 0 && compareStrength > 0) {
        return autobot;
      } else if (compareCourage < 0 && compareStrength < 0) {
        return decepticon
      }
    }
    if (Math.abs(compareSkill) >= 3) {
      return compareSkill > 0 ? autobot : decepticon;
    }
    if (autobot.getOverallRating() > decepticon.getOverallRating()) {
      return autobot;
    } else if (autobot.getOverallRating() < decepticon.getOverallRating()) {
      return decepticon;
    }
    return undefined;
  }

  onSubmit() {
    const transformer = new Transformer(
      this.transformerData.get('name')!.value,
      this.transformerData.get('team')!.value,
      this.transformerData.get('strength')!.value,
      this.transformerData.get('intelligence')!.value,
      this.transformerData.get('speed')!.value,
      this.transformerData.get('endurance')!.value,
      this.transformerData.get('rank')!.value,
      this.transformerData.get('courage')!.value,
      this.transformerData.get('firePower')!.value,
      this.transformerData.get('skill')!.value
    );

    if (transformer.team === 'A') {
      this.teamAutobot.push(transformer);
    } else {
      this.teamDecepticon.push(transformer);
    }
  }
}

export class Transformer {
  name: string;
  team: string;
  strength: number;
  intelligence: number;
  speed: number;
  endurance: number;
  rank: number;
  courage: number;
  firePower: number;
  skill: number;
  alive = true;

  constructor(name: string, team: string, strength: number, intelligence: number, speed: number,
    endurance: number, rank: number, courage: number, firePower: number, skill: number) {
    this.name = name;
    this.team = team;
    this.strength = strength;
    this.intelligence = intelligence;
    this.speed = speed;
    this.endurance = endurance;
    this.rank = rank;
    this.courage = courage;
    this.firePower = firePower;
    this.skill = skill;
  }

  getOverallRating() {
    return this.strength + this.intelligence + this.speed + this.endurance + this.firePower;
  }
}

export class AppConstants {
  public static AUTOBOT: 'A';
  public static DECEPTICON: 'D';
}
