import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartOneComponent } from './components/part-one/part-one.component';
import { PartTwoComponent } from './components/part-two/part-two.component';

const routes: Routes = [
  { path: 'part1', component: PartOneComponent },
  { path: 'part2', component: PartTwoComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
